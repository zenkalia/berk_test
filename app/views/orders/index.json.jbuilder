json.array!(@orders) do |order|
  json.extract! order, :id, :total_price, :num_units
  json.url order_url(order, format: :json)
end
