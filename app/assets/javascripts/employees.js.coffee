# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('#show_avg_button').on 'click', ->
    $.ajax({
      url: '/employees/avg_prices'
    }).done (data) ->
      $.each data, (index, datum) ->
        $('#avg_price_'+datum[0]).text(datum[1])
      $('.avg_price').css('display', 'inline')
      $('#show_avg_button').css('display', 'none')
    return false
  $('#hide_avg_button').on 'click', ->
    $('.avg_price').css('display', 'none')
    $('#show_avg_button').css('display', 'inline')
