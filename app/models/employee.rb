class Employee < ActiveRecord::Base
  has_many :orders

  def avg_unit_price_30
    num_orders = 0
    avg = 0.0
    self.orders.where('created_at > ?', DateTime.now - 30.days).each do |o|
      num_orders += 1
      avg += o.total_price / o.num_units
    end
    num_orders == 0 ? 'N/A' : (avg / num_orders).to_money.format
  end

  def self.avg_prices
    self.includes(:orders).map do |e|
      num_orders = 0
      total_price = 0.0
      e.orders.each do |o|
        num_orders += 1
        total_price += o.total_price
      end
      [ e.id, e.orders.any? ? (total_price/num_orders).to_money.format : 'N/A' ]
    end
  end

  def self.drop_down_options
    self.all.map do |e|
      [e.name, e.id]
    end
  end
end
