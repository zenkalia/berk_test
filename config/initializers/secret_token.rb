# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BerkTest::Application.config.secret_key_base = 'd41a4db2b9fb718dd176ef04645759225485515fc0c7ce08c6888bb5216723fdb756595a152113c96b0ed12e167354342621240c288019fdc42e3f0fdf6ff99f'
