class AddRelationToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :employee_id, :integer

    add_foreign_key(:orders, :employees)
  end
end
