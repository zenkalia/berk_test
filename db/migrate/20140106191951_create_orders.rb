class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.float :total_price
      t.integer :num_units

      t.timestamps
    end
  end
end
