require 'spec_helper'

describe Employee do

  describe '#drop_down_options' do
    subject { Employee.drop_down_options }
    describe 'with no employees' do
      it 'is an empty array' do
        expect(subject).to eq([])
      end
    end
    describe 'with some cool dudes' do
      let(:employee) { Employee.create(name: 'Ken') }
      let(:other_employee) { Employee.create(name: 'Mike') }

      before { employee; other_employee }

      it 'has two dudes' do
        expect(subject.count).to eq(2)
      end
      it 'has the right datas' do
        expect(subject).to eq([['Ken', employee.id], ['Mike', other_employee.id]])
      end
    end
  end
end

